import datetime
import json
import requests
import os
import sys
import time
from pushbullet import Pushbullet
from selectorlib import Extractor

pb = Pushbullet(os.environ["PUSHBULLET_API_KEY"])


def scrape_page(url):
    if "amazon" in url:
        service = "amazon"
    elif "bestbuy" in url:
        service = "bestbuy"
    elif "ComboDealDetails" in url:
        service = "newegg_combo"
    elif "newegg" in url:
        service = "newegg"
    elif "bh" in url:
        service = "bh"
    elif "staples" in url:
        service = "staples"
    elif "adorama" in url:
        service = "adorama"
    elif "officedepot" in url:
        service = "officedepot"

    e = Extractor.from_yaml_file("yamls/{}_product_page.yml".format(service))
    headers = {
        "dnt": "1",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36",
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "sec-fetch-site": "same-origin",
        "sec-fetch-mode": "navigate",
        "sec-fetch-user": "?1",
        "sec-fetch-dest": "document",
        "referer": "https://www.{}.com/".format(service.split("_")[0]),
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
        "Cache-Control": "no-cache",
        "Pragma": "no-cache",
    }
    # Download the page using requests
    r = requests.get(url, headers=headers)
    # Simple check to check if page was blocked (Usually 503)
    if r.status_code > 500:
        if "To discuss automated access to Amazon data please contact" in r.text:
            print("Page %s was blocked by Amazon. Please try using better proxies\n"%url)
        else:
            print("Page %s must have been blocked by Amazon as the status code was %d"%(url,r.status_code))
        return None
    # Pass the HTML of the page and create 
    return e.extract(r.text)


def raise_alert(url):
    pb.push_note("RTX 3080", url)


if __name__ == "__main__":
    count = 0
    while True:
        count += 1
        print("Beginning scanning iteration {} at {}...".format(count, datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
        with open("product_urls_to_search.txt","r") as urllist:
            for url in urllist.read().splitlines():
                if (len(url) == 0) or (url[0] == "#"):
                    continue
                # Change Amazon price to BuyButton and use the same logic for all
                try:
                    data = scrape_page(url)
                except:
                    error = sys.exc_info()[0]
                    print("ERROR reading {}: {}".format(url, error))
                    time.sleep(5)
                    continue
                if data:
                    # print(data)
                    if (data["BuyButton"] is not None) and ("SOLD OUT" not in data["BuyButton"].upper()) and ("SOON" not in data["BuyButton"].upper()) and ("NOTIFY" not in data["BuyButton"].upper()) and ("STOCK" not in data["BuyButton"].upper()) and ("NOT AVAILABLE" not in data["BuyButton"].upper()) and ("CHECK STORES" not in data["BuyButton"].upper()):
                        print("STOCK FOUND AT {}".format(url))
                        print("Pushing Bullet Alert...")
                        raise_alert(url)
                        print(data)
                        print("Resuming scan...")
                else:
                    print("No data found for {}".format(url))
